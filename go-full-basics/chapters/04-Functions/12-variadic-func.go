package main

import "fmt"

func main() {
	sum(2, 3, 5, 6)

	nums := []int{1, 2, 3}
	sum(nums...)
}

func sum(nums ...int) {
	fmt.Print("nums:", nums, " ")
	total := 0
	for _, add := range nums {
		total += add

	}
	fmt.Println(total)
}
