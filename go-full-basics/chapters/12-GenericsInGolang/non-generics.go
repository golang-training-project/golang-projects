package main

import "fmt"

func main() {
	// Intialize the map for hr intrger values
	ints := map[string]int64{
		"first":  34,
		"second": 12,
	}

	//Intialize a map for the float values
	floats := map[string]float64{
		"first":  35.98,
		"second": 26.99,
	}

	fmt.Printf("Non-Generic sums:%v and %v\n", SumInts(ints), SumFloats(floats))
}

func SumInts(m map[string]int64) int64 {
	var s int64
	for _, v := range m {
		s += v
	}
	return s
}

func SumFloats(m map[string]float64) float64 {
	var s float64
	for _, v := range m {
		s += v
	}
	return s
}
