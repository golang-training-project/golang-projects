package main

import "fmt"

func main() {
	nums := []int{2, 3, 4}
	sum := 0
	for _, i := range nums {
		sum += i
	}
	fmt.Println("sum:", sum)

	for index, i := range nums {
		if i == 3 {
			fmt.Println("key:", i, "value:", index)
		}
	}

	fruits := map[string]string{"a": "apple", "b": "banana"}
	for key, value := range fruits {
		fmt.Printf("%v -> %v \n", key, value)
	}

}
