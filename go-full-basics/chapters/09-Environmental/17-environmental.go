package main

import (
	"fmt"
	"os"
)

// Main function
func main() {

	// set environment variable TESTS
	os.Setenv("TEST", "tests")

	// returns value of TEST
	fmt.Println("TEST:", os.Getenv("TEST"))

	// Unset environment variable TEST
	os.Unsetenv("TEST")

	// returns empty string and false,
	// because we removed the test variable
	value, ok := os.LookupEnv("test")

	fmt.Println("TEST:", value, " Is present:", ok)

}
